#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QtCharts>
#include <QChartView>
#include<QLineSeries>
#include<vector>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	MainWindow(QWidget *parent = nullptr);
	~MainWindow();
	void processStart();
	std::vector<float> readFromFile(std::string fileName);
	void draw();
	int setFilePaths(std::string f0);


private:
	std::vector<float> Vpsnr_avg;
	std::vector<std::vector<float>> Vpsnr_tables;
	Ui::MainWindow *ui;

	QChart *chart ;
	QChartView *chartView ;
	QLineSeries *series;
	std::string fp0;
	QValueAxis *axisX ;
	QValueAxis *axisY;
	int k;
	std::vector<std::string> psnrNames ;

};
#endif // MAINWINDOW_H
