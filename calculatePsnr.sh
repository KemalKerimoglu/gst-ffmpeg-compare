#!/bin/bash
# calculates psnr for given encoded videos in arguement 1 and 2.
echo "Starting Calculating"
echo "Paths"
echo "Source File Directory:" $1 
echo "The directory that user wants:" $2


arg1=$(echo "$2")/gstBaseline.264
arg2=$(echo "$2")/gstMain.264
arg3=$(echo "$2")/gstHigh.264

echo $arg1
echo $arg2
echo $arg3


#arg=$(echo "$1" | cut -d "." -f 1)
#echo $arg

echo "***** Encoding For Baseline Profile *****"

gst-launch-1.0 -v filesrc location= $1 ! videoparse width=1920 height=1080 framerate=25/1 format=2 ! 'video/x-raw, format=(string)I420, width=(int)1920, height=(int)1080' ! omxh264enc profile=1 ! 'video/x-h264, streamformat=(string)byte-stream' ! h264parse ! qtmux ! filesink location=$arg1 -e

sleep 1

echo "***** Encoding For Main Profile *****"


gst-launch-1.0 -v filesrc location= $1 ! videoparse width=1920 height=1080 framerate=25/1 format=2 ! 'video/x-raw, format=(string)I420, width=(int)1920, height=(int)1080' ! omxh264enc profile=2 ! 'video/x-h264, streamformat=(string)byte-stream' ! h264parse ! qtmux ! filesink location=$arg2 -e

sleep 1

echo "***** Encoding For High Profile *****"

gst-launch-1.0 -v filesrc location= $1 ! videoparse width=1920 height=1080 framerate=25/1 format=2 ! 'video/x-raw, format=(string)I420, width=(int)1920, height=(int)1080' !omxh264enc profile=8 ! 'video/x-h264, streamformat=(string)byte-stream' ! h264parse ! qtmux ! filesink location=$arg3 -e

sleep 3

echo "====== Comparing.. ======"

ffmpeg -i $arg1 -s 1920x1080 -framerate 25 -i $1 -lavfi psnr=stats_file=$2/psnr_logfile_Baseline.txt -f null -
ffmpeg -i $arg2 -s 1920x1080 -framerate 25 -i $1 -lavfi psnr=stats_file=$2/psnr_logfile_Main.txt -f null -
ffmpeg -i $arg3 -s 1920x1080 -framerate 25 -i $1 -lavfi psnr=stats_file=$2/psnr_logfile_High.txt -f null -


#ffmpeg -s 1920x1080 -framerate 25 -i $1 -s 1920x1080 -framerate 25 -i $1 -lavfi psnr=stats_file=/home/nvidia/kemal/gst-ffmpeg-compare/psnr_logfile.txt -f null -

echo "Calculating Done"
