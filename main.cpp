#include "mainwindow.h"

#include <QApplication>

int main(int argc, char *argv[])
{
	QApplication a(argc, argv);
	MainWindow w;
	if (argc<2) {
		qDebug("Please enter directory that you saved files\n"
			   "Example: ./qtchart  home/user/Document ");
		std::terminate();

	}

	else if (argc>2 ){
		qDebug("Please  enter only one argument\n" );
		std::terminate();
	}

//	else {
		qDebug("Calculating PSNR..");

		if (w.setFilePaths(argv[1] )){
			w.processStart();
			w.show();
		}
//	}
	return a.exec();
}
