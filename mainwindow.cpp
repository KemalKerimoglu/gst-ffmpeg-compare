#include "mainwindow.h"
#include "ui_mainwindow.h"
#include<iostream>
#include<fstream>
#include<QDebug>

using namespace std;


MainWindow::MainWindow(QWidget *parent)
	: QMainWindow(parent)
	, ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	chart = new QChart();
	chartView = new QChartView(chart);
	axisX = new QValueAxis();
	axisY = new QValueAxis();
	psnrNames ={"/psnr_logfile_Baseline.txt","/psnr_logfile_Main.txt","/psnr_logfile_High.txt"};

}

MainWindow::~MainWindow()
{
	delete axisX;
	delete axisY;
	delete series;
	delete chartView;
	delete chart;
	delete ui;
	Vpsnr_tables.clear();
}

void MainWindow::processStart()
{


	for (k=0 ; k<psnrNames.size() ; k++) {
		Vpsnr_tables.push_back(readFromFile(fp0+psnrNames.at(k)));
	}

	if (!(Vpsnr_tables.empty()))
	{	qDebug("Reading process finished");
		draw();
	}
	else qDebug()<<"Files couldn't read";
}

std::vector<float> MainWindow::readFromFile(std::string fileName)
{
	Vpsnr_avg.clear();
	ifstream in;

	if (!in.good()) {
		qDebug("Ain't no file");
		return std::vector<float>();
	}
	qDebug("Reading");
	in.open(fileName);
	while(!in.eof()){
		string line;
		getline(in,line);
		QString qLine = QString::fromStdString(line);
		QStringList s = qLine.split("psnr_avg:");
		if (s.size() > 1) {
			QString s0 = s[1].split(" ")[0];
			Vpsnr_avg.push_back(s0.toFloat());
		}
	}

	in.close();

	return Vpsnr_avg;
}

void MainWindow::draw()
{
	int j=0;

	for (auto v : Vpsnr_tables) {

		series = new QLineSeries();

		for (int i = 0; i < v.size(); i++) {
			series->append(i, v.at(i));
		}

		if (j==0) series->setName("Baseline");
		if (j==1) series->setName("Main");
		if (j==2) series->setName("High");

		chart->addSeries(series);

		j++;
	}

	//	chart->legend()->hide();
	chart->legend()->setVisible(true);
	chart->legend()->setAlignment(Qt::AlignBottom);

	chart->setTitle("Comparing Two Encoded Video");

	//			chart->createDefaultAxes();

	axisX->setTitleText("Frames");
	axisX->setLabelFormat("%i");
	chart->addAxis(axisX, Qt::AlignBottom);
	series->attachAxis(axisX);

	axisY->setTitleText("PSNR");
	axisY->setLabelFormat("%.2f");
	chart->addAxis(axisY, Qt::AlignLeft);
	series->attachAxis(axisY);

	chartView->setRenderHint(QPainter::Antialiasing);
	QMainWindow window;
	window.setCentralWidget(chartView);
	chart->setMinimumSize(783,583);
	ui->horizontalFrame->resize(793,593);
	window.show();
	chartView->setParent(ui->horizontalFrame);

	QPixmap p = chartView->grab();
	p.save("pf0/graphic.png", "PNG");

}

int  MainWindow::setFilePaths(std::string f0)
{

	fp0=f0;

	return 1;
}




